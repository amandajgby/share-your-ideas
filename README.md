# Share Your Ideas

Simple CRUD with Express.js. Digunakan untuk mencatat ide apa pun yang dimiliki.

# How to start project

1. Clone project
2. Import database dari file .sql yang terdapat di dalam folder.
3. Masuk ke folder project dan jalankan perintah "npm install" di terminal.
4. Jalankan perintah "node index" di terminal.
5. Buka browser dan ketikkan url berikut: http://localhost:8000/

# Login Credentials

**Username:** admin

**Password:** password

# index.js

Digunakan untuk meng-handle semua routes.
