//path module
const path = require('path');
//express module
const express = require('express');
//hbs view engine
const hbs = require('hbs');
//cookieParser
const cookieParser = require('cookie-parser');
//bodyParser middleware
const bodyParser = require('body-parser');
//swagger ui
const swaggerUi = require('swagger-ui-express');
swaggerDocument = require('./swagger.json');
//mysql database
const mysql = require('mysql');

const app = express();
 
//Create connection
const conn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'idea'
});
 
//connect to database
conn.connect((err) =>{
  if(err) throw err;
  console.log('Mysql Connected...');
});
 
//set views file
app.set('views',path.join(__dirname,'views'));
//set view engine
app.set('view engine', 'hbs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//set public folder as static folder for static file
app.use('/assets',express.static(__dirname + '/public'));
//cookieParser
app.use(cookieParser());
app.use((req, res, next) => {
  //get auth token from cookie
  const authToken = req.cookies['AuthToken'];
  //inject user to request
  req.user = authTokens[authToken];
  next();
});

//Hashed Password
const crypto = require('crypto');
const getHashedPassword = (password) => {
  const sha256 = crypto.createHash('sha256');
  const hash = sha256.update(password).digest('base64');
  return hash;
}

const users = [
  {
      username: 'admin',
      // This is the SHA256 hash for value of `password`
      password: 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg='
  }
];

//route for login page
app.get('/',(req, res) => {
    res.render('login');
});

const generateAuthToken = () => {
  return crypto.randomBytes(30).toString('hex');
}

//route for login process
const authTokens = {};
app.post('/auth',(req,res) => {
  const {username, password} = req.body;
  const hashedPassword = getHashedPassword(password);

  const user = users.find(u => {
    return u.username === username && hashedPassword === u.password
  });

  if(user) {
    const authToken = generateAuthToken();
    //store token
    authTokens[authToken] = user;
    //set token in cookie
    res.cookie('AuthToken',authToken);
    //redirect user to dashboard
    res.redirect('/home');
  }
  else {
    res.render('login', {
      message: 'Invalid username or password',
      messageClass: 'alert-danger'
    });
  }
});

//auth middleware
const requireAuth = (req, res, next) => {
  if(req.user) {
    next();
  }
  else {
    res.render('login', {
      message: 'Please login to continue.',
      messageClass: 'alert-danger'
    });
  }
};

  //route for data list
  app.get('/home', requireAuth, (req, res) => {
    let sql = "SELECT * FROM idea";
    let query = conn.query(sql, (err, results) => {
      if(err) throw err;
      res.render('view',{
        results: results
      });
    });
  });
   
  //route for insert data
  app.post('/save',(req, res) => {
    let data = {name: req.body.name, description: req.body.description};
    let sql = "INSERT INTO idea SET ?";
    let query = conn.query(sql, data,(err, results) => {
      if(err) throw err;
      res.redirect('/home');
    });
  });

   //route for detail data
   app.get('/detail',(req, res) => {
    let sql = "SELECT idea SET name='"+req.body.name+"', description='"+req.body.description+"' WHERE id="+req.body.id;
    let query = conn.query(sql, (err, results) => {
      if(err) throw err;
      res.redirect('/home');
    });
  });
   
  //route for update data
  app.post('/update',(req, res) => {
    let sql = "UPDATE idea SET name='"+req.body.name+"', description='"+req.body.description+"' WHERE id="+req.body.id;
    let query = conn.query(sql, (err, results) => {
      if(err) throw err;
      res.redirect('/home');
    });
  });
   
  //route for delete data
  app.post('/delete',(req, res) => {
    let sql = "DELETE FROM idea WHERE id="+req.body.id+"";
    let query = conn.query(sql, (err, results) => {
      if(err) throw err;
        res.redirect('/home');
    });
  });

//route for logout
app.get('/logout', (req, res) => {
  if(req.user) {
    res.clearCookie('AuthToken');
    res.render('login', {
      message: 'You are successfully logged out.',
      messageClass: 'alert-success'
    });
  }
  else {
    res.send('Logout failed.');
  }
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

//server listening
app.listen(8000, () => {
    console.log('Sever is running at port 8000');
});